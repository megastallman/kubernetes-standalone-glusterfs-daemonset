#!/bin/bash

kubectl create ns standalone-glusterfs || true
kubectl -n standalone-glusterfs apply -f standalone-glusterfs-daemonset.yaml

echo "Waiting for a minute for daemonset to stabilize..."
sleep 60

ADDR_LIST=""

GLUSTER_IP_POD_LIST=$(kubectl -n standalone-glusterfs get pods -o wide | grep -v NAME | awk '{print $6,$1}' | sort | sed 's/ standalone-glusterfs/standalone-glusterfs/')

echo "We've got the following GlusterFS pods included:"

for i in $GLUSTER_IP_POD_LIST
do
    POD_NAME=$(echo $i | sed 's/standalone-glusterfs/ standalone-glusterfs/' | cut -f2 -d' ')
    POD_ADDR=$(echo $i | sed 's/standalone-glusterfs/ standalone-glusterfs/' | cut -f1 -d' ')
    echo "${POD_NAME}(${POD_ADDR})"
    ADDR_LIST="$ADDR_LIST $POD_ADDR"
    LAST_POD=$POD_NAME
done

#echo "With the following addresses: ${ADDR_LIST}"
echo "Using ${LAST_POD} to build a GlusterFS cluster"

for ADDR in $ADDR_LIST
do
    kubectl -n standalone-glusterfs exec -ti ${LAST_POD} -- gluster peer probe ${ADDR}
done
