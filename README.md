# Kubernetes Standalone GlusterFS deploy script

Deploy and/or rebuild your Standalone GlusterFS cluster with `./deploy.sh` script. 
This script is idempotent, but is enough to be run just once. Tested on Openshift(with privileged containers enabled) and Rancher(RKE). Should work on any other Kubernetes distribution. To consume a GlusterFS volume, use `test-storage.yaml` as a reference.

#### Glusterfs troubleshooting guide:
1) $ `kubectl -n standalone-glusterfs get pods`
A daemonset is being spread across all cluster nodes
2) $ `kubectl -n standalone-glusterfs exec -ti standalone-glusterfs-<ANY_POD> gluster peer status`
See that all pods are cluster members. The "9 peers" message means you've got a 10-node daemonset and a current pod has 9 friends.
3) $ `kubectl -n standalone-glusterfs exec -ti standalone-glusterfs-<ANY_POD> gluster volume info`
No volumes present(Or see the volume list)

#### Glusterfs operations guide:
1) $ `kubectl -n standalone-glusterfs exec -ti standalone-glusterfs-<ANY_POD> bash`
Get inside of any GlusterFS pod and operate the cluster from inside it.
2) Create a redundant volume: # `gluster volume create test3rep replica 3 10.10.9.9:/mnt/brick/test3rep 10.10.9.8:/mnt/brick/test3rep 10.10.9.7:/mnt/brick/test3rep`
3) Start it: # `gluster volume start test3rep`
4) Create a non-redundant volume: # `gluster volume create test1rep 10.10.9.9:/mnt/brick/test1rep`
5) Start it: # `gluster volume start test1rep`
6) List all volumes: # `gluster volume info` 
7) Mount volume on any external machine: # `mount -t glusterfs 10.10.9.1:/test1rep /your_mountpoint`
